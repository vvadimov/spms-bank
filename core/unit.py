from core.pioneer import Pioneer

class PioneerError(Exception):
	pass

class Unit:
	def __init__(self, cfg):
		self.pioneers = {}
		self.cfg      = cfg

	def addPioneer(self, name, grade):
		if (name in self.pioneers):
			raise PioneerError()
		else:
			self.pioneers[name] = Pioneer(grade, self.cfg)

	def delPioneer(self, name):
		try:
			self.pioneers.pop(name)
		except KeyError:
			raise PioneerError()

	def listPioneers(self):
		return list(self.pioneers.keys())

	def getPioneerBalance(self, name):
		try:
			return self.pioneers[name].getBalance()
		except KeyError:
			raise PioneerError()

	def payPioneer(self, name, value):
		try:
			self.pioneers[name].pay(value)
		except KeyError:
			raise PioneerError()
