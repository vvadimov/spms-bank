class Pioneer:
	def __init__(self, grade, cfg):
		self.cfg     = cfg
		self.balance = float(self.cfg["DEFAULT"]["StartBalance"]) #todo
		self.grade   = grade
		self.req     = {}

	def pay(self, value):
		self.balance += value

	def getBalance(self):
		return self.balance

	def getGrade(self):
		return self.grade
	
	def setReq(self, action, value):
		self.req[action] = value

	def getReq(self, action):
		return self.req[action]
