import configparser as cfgp
import os
from core.unit import Unit
import core.requirements as req

commonConf = cfgp.ConfigParser()
commonConf.read(os.path.join("cfg", "common.cfg"))

units = []
for i in range(int(commonConf["DEFAULT"]["Units"])):
	units.append(Unit(commonConf))

reqConf = {}
for action in req.reqs:
	reqConf[action] = cfgp.ConfigParser()
	reqConf[action].read(os.path.join("cfg", action + ".cfg"))

def dailyTax():
	for unit in units:
		for name in unit.listPioneers():
			unit.payPioneer(name, -float(commonConf["DEFAULT"]["DailyTax"]))

def pay(args):
	for record in args:
		(where, who, howMany) = record
		units[where - 1].payPioneer(who, howMany)

def fine(args):
	for record in args:
		(where, who, howMany) = record
		units[where - 1].payPioneer(who, -howMany)

def addPioneer(args):
	for record in args:
		(where, who, grade) = record
		units[where - 1].addPioneer(who, grade)

def delPioneer(args):
	for record in args:
		(where, who) = record
		units[where - 1].delPioneer(who)

def required(action, args):

	pass

def judgementDay():
	pass
